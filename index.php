<?php
session_start();



if(!isset($_SESSION['final_price'])){
	$_SESSION['final_price'] = 0;
}


$servername = "localhost";
$username = "root";
$password = "root";

try{
	$conn = new PDO("mysql:host=$servername;dbname=sazzad", $username, $password);
	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}catch(PDOException $e){
	echo "Connection failed: " . $e->getMessage();
}
	
	function getProductDetails($id,$qty){

		$servername = "localhost";
		$username = "root";
		$password = "root";

		try{
			$conn = new PDO("mysql:host=$servername;dbname=sazzad", $username, $password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
			echo "Connection failed: " . $e->getMessage();
		}

		$stmt = $conn->prepare("SELECT prod_name,prod_price FROM products WHERE id = :id");
		$stmt->bindParam(':id',$id);
		$stmt->execute();
		$row = $stmt->fetchObject();

		$prod_name = $row->prod_name;
		$prod_price = $row->prod_price;
		$final_price = $qty * $prod_price;

		return array('prod_name'=>$prod_name,
				'prod_price'=>$final_price
				);	


	}

	function getProductPrice($id){

		$servername = "localhost";
		$username = "root";
		$password = "root";

		try{
			$conn = new PDO("mysql:host=$servername;dbname=sazzad", $username, $password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
			echo "Connection failed: " . $e->getMessage();
		}

		$stmt = $conn->prepare("SELECT prod_price FROM products WHERE id = :id");
		$stmt->bindParam(':id',$id);
		$stmt->execute();
		$row = $stmt->fetchObject();

		
		return $row->prod_price;
		

		


	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>
		Simple Cart
	</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	
</head>
<body>
	<div class="container" style="margin-top: 100px">
		<div class="row">
			<div class="col-md-6">
				<table class="table">
					<?php
						  $stmt = $conn->prepare("SELECT * FROM products");
						  $stmt->execute();
						  while($result = $stmt->fetchObject()){
						  	echo "<tr>
						  			<td>{$result->prod_name}</td>
						  			<td>₹ {$result->prod_price}</td>
						  			<td><a class='btn btn-primary btn-sm' href='?add={$result->id}'>+</a></td>

						  		  </tr>";
						  }
					?>
				</table>
				
			</div>
			<div class="col-md-4 col-md-offset-2" style="margin-top: -80px">
				<h1 class="text-center">
					Cart Items
				</h1>
				
				<?php
					if($_SESSION['cart_items'] == ''){
						echo "<h6 class='text-center'>Cart Empty</h6>";
					}else{

						$data = $_SESSION['cart_items'];
						echo"<table class='table '>";
						foreach ( $data as $p_id => $p_qty) {
							echo"<tr><td>";
							$pdata = getProductDetails($p_id,$p_qty);
							echo '<a href="?add='.$p_id.'" class="btn btn-primary btn-sm">+</a>
								  <a href="?remove='.$p_id.'" class="btn btn-danger btn-sm">-</a>     
								  &nbsp; &nbsp;'. 
								  $pdata['prod_name'].' x '. $p_qty . ' = ₹ ' . $pdata['prod_price'];
							echo "</tr></td>";

							
						}
						echo"</table> <br>";	
					}

					if($_SESSION['cart_items'] != ''){
						echo'<strong>Total: ₹ '.$_SESSION['final_price'].'</strong>
						<br>
						
						<br><a class="btn btn-danger" href="?cart=clear">Clear</a>';
					}
				?>

			</div>
<?php

	if(isset($_GET['add'])){

		$id=intval($_GET['add']); 
		$_SESSION['cart_items'][$id]++;
		$_SESSION['final_price'] +=  getProductPrice($id);
		
		?>
		<script>window.location='index.php';</script>
		<?php
	}

	if(isset($_GET['remove'])){


		$id=intval($_GET['remove']); 
		$_SESSION['cart_items'][$id]--;
		
		if($_SESSION['cart_items'][$id] == 0){
			unset($_SESSION['cart_items'][$id]);
		}

		$_SESSION['final_price'] -=  getProductPrice($id);
		
		?>
		<script>window.location='index.php';</script>
		<?php
	}

	if(isset($_GET['cart'])){

		$_SESSION['cart_items'] = '';	
		$_SESSION['final_price'] = 0;
		
		?>

		<script>window.location='index.php';</script>
		
		<?php				

	}

?>
		</div>
	</div>
</body>
</html>